'use strict';

const functions = require('firebase-functions');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var Clarifai = require('clarifai');

const app = new Clarifai.App({
 apiKey: {CLARIFAI_API_KEY_}
});
const freeFoodModel = {MODEL_NAME}; // eg "Free Food"
const chatEndpoint = {COMPANY_CHAT_ENDPOINT} // endpoint for slack, hipchat, mattermost, etc.
const firebaseAppName = {FIREBASE_APP_NAME}

exports.postFoodToCompanyChat = functions.storage.object().onChange(event => {
    var photoUrl = "https://firebasestorage.googleapis.com/v0/b/" + firebaseAppName + ".appspot.com/o/photos_cropped%2F" + event.data.name.split("/")[1] + "?alt=media&token=" + event.data.metadata.firebaseStorageDownloadTokens + ".jpg";
    return getFoodsFromPhoto(photoUrl).then(foods => {
      sendMessageToChat(getFoodString(foods), photoUrl);
    });
});

function sendMessageToChat(foodString, photoUrl){
  var params = JSON.stringify({text:"@all Free food in the kitchen!\n" + photoUrl + "\nI think the food might be:" + foodString + "\nBut I'm still learning!", username:"Free Food Bot"}); // @TODO replace this JSON blob with one that is appropriate to your application
  var xhr = new XMLHttpRequest();
  xhr.open("POST", chatEndpoint, true);
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.send(params);
}

function getFoodsFromPhoto(photoUrl) {
  return app.models.predict(freeFoodModel, photoUrl).then(
    function(response) {
      return response.outputs[0].data.concepts;
    },
    function(err) {
      console.warn(stringifyErr(err));
    }
  );
}

const maxFoodsReported = 5
const foodRecognitionThreshold = .5
function getFoodString(foods){
  var output = '';
  for (var i=0; (i < maxFoodsReported && i < foods.length && foods[i].value > foodRecognitionThreshold); i++) {
    output += "\n" + food.name + " (" + food.value + ")";
  }
  if (output.length == 0){
    output = "\nI couldn't recognize anything...";
  }
  return output;
}

function stringifyErr (object) {
    var simpleObject = {};
    for (var prop in object ){
        if (!object.hasOwnProperty(prop)){
            continue;
        }
        if (typeof(object[prop]) == 'object'){
            continue;
        }
        if (typeof(object[prop]) == 'function'){
            continue;
        }
        simpleObject[prop] = object[prop];
    }
    return JSON.stringify(simpleObject);
};