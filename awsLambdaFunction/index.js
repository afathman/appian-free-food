var AWS = require('aws-sdk');
var request = require('request');
AWS.config.update({region:'us-east-1'});
var IFTTTkey = process.env['iftttMakerChannelKey']; //@TODO must have this defined in the environment variables

exports.handler = function(event, context) {
    request('https://maker.ifttt.com/trigger/' + 'AWS-'+ event.clickType + '/with/key/' + IFTTTkey, function (error, response, body) {
        console.log("Complete! Response: ", response.statusCode);
    }
)};
